#include <QCoreApplication>

#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    setlocale(LC_ALL,"Russian");

    EnergyDispatcherServer edServer;

    // Вызов иницализации сервера!
    edServer.initServer();

    // Вызов старта сервера!
    edServer.startServer();

    return a.exec();
}
