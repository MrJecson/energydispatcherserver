#include "server.h"

#include <QTcpSocket>
#include <QDebug>

//#include "stdafx.h"
#include <iostream>
#include <stdio.h>


//------------------------------------------------------------------------------
// Конструктор
//------------------------------------------------------------------------------
EnergyDispatcherServer::EnergyDispatcherServer(QObject *parent) : QTcpServer(parent)
{    

}

//------------------------------------------------------------------------------
// Деструктор
//------------------------------------------------------------------------------
EnergyDispatcherServer::~EnergyDispatcherServer()
{

}

//------------------------------------------------------------------------------
// Инициализация Сервера!
//------------------------------------------------------------------------------
void EnergyDispatcherServer::initServer()
{
//------------------------------------------------
//Начальное определение параметров структуры
//    server_conf.port = 1992;
//    server_conf.timer_interval = 1000;
//------------------------------------------------


// Будет читка из файла cfg! ??????


//    CfgReader cfg;
//        if (cfg.load("../cfg/tcp-config.xml"))
//        {
//            cfg.getString("TcpConfig", "Host", sim_conf.hostName);
//            int iprt = 1992;
//            cfg.getInt("TcpConfig", "Port", iprt);
//            sim_conf.port = static_cast<quint16>(iprt);
//            cfg.getInt("TcpConfig", "ReconnectInterval", sim_conf.reconnectInterval);
//        }


//----------------------------------------------------------------------------------------
//    connect(this, &EnergyDispatcherServer::newConnection,
//            this, &EnergyDispatcherServer::slotClientAutorized);

//    connect(client, &QTcpSocket::disconnected,
//            this, &EnergyDispatcherServer::slotClientDisconnected);
//----------------------------------------------------------------------------------------


    // Костыль без CfgReader!
    server_conf.port = 1992;
}

//------------------------------------------------------------------------------
// Старт Сервера!
//------------------------------------------------------------------------------
void EnergyDispatcherServer::startServer()
{  
    connect(this, &EnergyDispatcherServer::newConnection, this, &EnergyDispatcherServer::slotClientAutorized);

    if (!this->isListening())
    {
        if(this->listen(QHostAddress::Any, server_conf.port))
        {
            qDebug() << "Listening to port connection:" << server_conf.port;
        }
        else
        {
            qDebug() << "Listening to the communication port" << server_conf.port << "is not active!";
        }
    }
}

//------------------------------------------------------------------------------
// Слот Авторизации Клиента на Cервере!
//------------------------------------------------------------------------------
void EnergyDispatcherServer::slotClientAutorized()
{
    client = this->nextPendingConnection();

    connect(client, &QTcpSocket::disconnected, this, &EnergyDispatcherServer::slotClientDisconnected);

    qDebug() << "Energy Dispatcher status:" << client->state() << "," << "local address:" << client->peerAddress();

    /// Сигнал/Слот запуска таймера обмена данными!
    connect(&edTimer, &QTimer::timeout, this, &EnergyDispatcherServer::tcpFeedBack);

    /// Запуск таймера
    edTimer.start(server_conf.timer_interval);
}

//------------------------------------------------------------------------------
// Слот Дисконекта Клиента на сервере!
//------------------------------------------------------------------------------
void EnergyDispatcherServer::slotClientDisconnected()
{
    qDebug() << "Energy Dispatcher status:" << client->state() << "," << "local address:" << client->peerAddress();
}

//------------------------------------------------------------------------------
// Обмен данных с клиентом!
//------------------------------------------------------------------------------
void EnergyDispatcherServer::tcpFeedBack()
{
    // Проверка на подключенных клиентов!
    if (client->state() == QTcpSocket::UnconnectedState)
    {
        return;
    }

    // Получаем данные с клиента в сыром виде!
    QByteArray input_data = client->readAll();

    // Проверка на соответствие размерностей!
    if (static_cast<size_t>(input_data.size()) != sizeof (data_client_t))
    {
        qDebug() << "Invalid data size!";
    }

    // Переобразуем полученные данные!
    data_client = *static_cast<data_client_t *>(static_cast<void *>(input_data.data()));

    // Отправка данных клиенту!
    QByteArray output_data;
    output_data.resize(sizeof (data_client_t));

    output_data = this->data_client.serialize();

    // Проверка на открытый сокет клиента!
    if (client->isOpen())
    {
        // Передаем данные клиенту!
        client->write(output_data);

        // Запись из буфера в сетевой сокет! (Проверка на передачу данных, если ничего не передано, то false!)
        client->flush();
    }
}
