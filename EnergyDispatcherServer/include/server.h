#ifndef SERVER_H
#define SERVER_H

#include <QtGlobal>
#include <QObject>
#include <QTimer>
#include <QTcpServer>
#include <QList>

#include <QDataStream>


//Структура конфигураций сервера (Возможны изменения!)
struct server_config_t
{
    /// Порт Сервера!
    quint16 port;

    int  timer_interval;

    server_config_t()
        : port(1992u)
        , timer_interval(500)
    {

    }

};

//Структура данных общения с клиентом (Будут изменения!)
#pragma pack(push, 1)
struct data_client_t
{

    QMap<QString, data_client_t>   state;

    /// Имя объекта передоваемое от клиента! (SVG файл!)
    QString     nameObjectSVG;

    /// Состояние объекта возвращаемое сервером!
    bool    stateObjectSVG;

    data_client_t()
        : stateObjectSVG(false)
    {

    }

    QByteArray serialize()
    {
        QByteArray data;
        QDataStream data_s(&data, QIODevice::WriteOnly);

        for (data_client_t data_clnt : state)
        {
            data_s << data_clnt.nameObjectSVG
                   << data_clnt.stateObjectSVG;
        }

        return data;
    }
};
#pragma pack(pop)

//----------------------------------------------------------------

/// Класс Сервер Энергодиспетчера
class EnergyDispatcherServer : public QTcpServer
{
    Q_OBJECT

public:

    EnergyDispatcherServer(QObject *parent = Q_NULLPTR);

    ~EnergyDispatcherServer();

    /// Инициализация
    void initServer();

    /// Метод старта сервера!
    void startServer();

private:

    /// Конфигурации сервера!
    server_config_t server_conf;

    /// Данные клиента!
    data_client_t   data_client;

    /// Таймер обмена данными!
    QTimer  edTimer;

    /// Сокет клиент!
    QTcpSocket  *client;

public slots:

    void    slotClientAutorized();

    void    slotClientDisconnected();

    void    tcpFeedBack();
};

#endif // SERVER_H
